<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<link rel="stylesheet" href="/static/plat/css/jquery.mobile-1.4.5.min.css" />
	<script src="/static/plat/js/jquery-1.11.1.min.js"></script>
	<script src="/static/plat/js/jquery.mobile-1.4.5.min.js"></script>
</head>
<body>
      <div data-role="page" id="main">
        <div data-role="header">
            <a href="#login" data-role="button" data-icon="home" data-transition="fade">登录</a>
        <h1>Mogo暗黑战神</h1>
<a href="#register" data-role="button" data-icon="user" data-transition="fade">注册</a>
<div data-role="navbar">
                        <ul>
                                <li><a href="#qq">玩家qq群:66574439</a></li>
                                <li><a href="#pay">充值</a></li>
                        </ul>
                </div>
        </div>
        <div data-role="content">
           <span>Mogo暗黑战神版本新建，提供最畅快的战斗体验，提供pvp系统方便随时战斗。首款3D动作手机网游，采用俯视角即时战斗模式, 使用Unity3D引擎,打造国际品质的游戏画面。</span>
	      
	   <div>
	</div>	
       </div>
    </div>  
      <div data-role="page" id="login">
         <div data-role="header">
	    <a href="#main" data-role="button" data-icon="back" data-transition="fade">返回</a>
            <h1>用户登录</h1>
	<div data-role="navbar">
                        <ul>
                                <li><a href="#qq">玩家qq群:66574439</a></li>
                                <li><a href="#pay">充值</a></li>
                        </ul>
                </div>
        </div>
        <div data-role="content">
            <form id="loginForm">
              <div data-role="fieldcontain">
                <input type="text" name="username" id="username" placeholder="用户名"/>
                <input type="password" name="password" id="password" placeholder="密码"/>
                <button id="btnLogin">登录</button>
              </div>
            </form>
                <p>没有账号？<a href="#register" data-transition="slide">点击注册</a></p>
        </div>
    </div>
        
    <div data-role="page" id="register">
        <div data-role="header">
            <a href="/plat" data-role="button" data-icon="back" data-transition="fade">返回</a>
            <h1>用户注册</h1>
<div data-role="navbar">
                        <ul>
                                <li><a href="#qq">玩家qq群:66574439</a></li>
                                <li><a href="#pay">充值</a></li>
                        </ul>
                </div>
        </div>
        <div data-role="content">
            <form id="registerForm">
                <div data-role="fieldcontain">
                    <input type="text" name="username" id="username" placeholder="用户名"/>
                    <input type="password" name="password" id="password" placeholder="密码"/>
                    <input type="password" id="repassword" placeholder="确认密码"/>
                    <input type="email" name="email" id="email" placeholder="邮箱"/>
		    <input type="text" name="recommend" id="recommend" placeholder="填写推荐人，可为空"/>
                    <button  id="btnRegister">注册</button>
                </div>
            </form>
        </div>
    </div>

   <div data-role="page" id="lancher">
        <div data-role="header">
            <a href="/plat" data-role="button" data-icon="back" data-transition="fade">返回</a>
	<h1>启动游戏</h1>
<div data-role="navbar">
                        <ul>
                                <li><a href="#qq">玩家qq群:66574439</a></li>
                                <li><a href="#pay">充值</a></li>
                        </ul>
                </div>
        </div>
        <div data-role="content">
	   <div id="welcome"> 请登录..</div>
           <a href="#" class="ui-btn ui-btn-inline" id="startup">启动游戏</a>
           <a href="/plat" class="ui-btn ui-btn-inline" id="logout">退出登录</a>
	</div>
    </div>
<div data-role="dialog" id="error">

    <!-- 页眉部分start -->
    <div data-role="header">
       <h1>提示信息</h1>
    </div>
    <!-- 页眉部分end -->
    
    <!-- content部分start -->
    <div data-role="content">
        <p id="errort">对不起，操作错误，请重试！</p>
    </div>
    <!-- content部分end -->

</div>

<div data-role="dialog" id="error">

    <!-- 页眉部分start -->
    <div data-role="header">
       <h1>提示信息</h1>
    </div>
    <!-- 页眉部分end -->

    <!-- content部分start -->
    <div data-role="content">
        <p id="errort">对不起，操作错误，请重试！</p>
    </div>
    <!-- content部分end -->

</div>

<div data-role="dialog" id="qq">

    <!-- 页眉部分start -->
    <div data-role="header">
       <h1>加入qq群，有好礼相送</h1>
    </div>
    <!-- 页眉部分end -->

    <!-- content部分start -->
    <div data-role="content">
        <p id="errort">快来加入我们的群吧，66574439，来就送啊</p>
    </div>
    <!-- content部分end -->

</div>

<div data-role="dialog" id="pay">

    <!-- 页眉部分start -->
    <div data-role="header">
       <h1>充值提示</h1>
    </div>
    <!-- 页眉部分end -->

    <!-- content部分start -->
    <div data-role="content">
        <p id="errort">在游戏中，点击充值字样的按钮，直接冲入游戏的角色信息中，欢迎使用！</p>
    </div>
    <!-- content部分end -->

</div>


<div data-role="footer" data-position="fixed" role="contentinfo" class="ui-footer ui-bar-inherit ui-footer-fixed slideup">
		<div data-role="navbar" class="ui-navbar" role="navigation">
			<ul class="ui-grid-b">
                               <li class="ui-block-a"><a href="/plat" class="ui-link ui-btn">首页</a></li> 
				<li class="ui-block-b"><a href="#lancher" class="ui-link ui-btn">开始游戏</a></li>
                                <li class="ui-block-c"><a href="uniwebview://close" class="ui-link ui-btn">关闭窗口</a></li>
			</ul>
		</div>
	</div>
<script type="text/javascript"
src="/static/plat/js/common.js">
</script>

<script type="text/javascript">

 function onSuccess(data, status)
        {
	   var json_data = eval("("+data+")");
	    if(json_data.code==200){
  	        var data = json_data.data;	
		setCookie("account",data.username);
	        setCookie('suid',data.id);
   		setCookie('sign',json_data.sign);	
		setCookie('timestamp',json_data.timestamp);
	        window.location.href='/plat';
	    }else{
		document.getElementById("errort").innerHTML="服务器返回："+json_data.msg;
                window.location.href='#error';	       	
            }
        }
  
        function onError(data, status)
        {
	   document.getElementById("errort").innerHTML="服务器返回错误";
                window.location.href='#error';
        }
     


  $(document).ready(function() {
    if(getCookie('account')!=null){
	if(parseInt(getCookie('timestamp'))+1000<parseInt($.now()/1000)){
		delCookie('account');
		window.location.href='#login';
	}else{
        	window.location.href='#lancher';
        	document.getElementById('startup').href="uniwebview://startup?account="+getCookie('account')
			+ "&suid="+getCookie('suid')+"&sign="+getCookie('sign')+"&timestamp="+getCookie('timestamp')+"&token=1";
        	document.getElementById('welcome').innerHTML="<h3>欢迎您："+ getCookie('account')+"</h3>";
	}
    }else{
	delCookie('account');
        window.location.href="#login";
    }        
    $("#logout").click(function(){
	delCookie('account');	
    });
    $("#btnLogin").click(function(){
                var formData = $("#loginForm").serialize();
  
                $.ajax({
                    type: "POST",
                    url: "/plat/login.php",
                    cache: false,
                    data: formData,
                    success: onSuccess,
                    error: onError
                });
  
                return false;
            });

	  $("#btnRegister").click(function(){
  		console.log("register");
                var formData = $("#registerForm").serialize();
  
                $.ajax({
                    type: "POST",
                    url: "/plat/register.php",
                    cache: false,
                    data: formData,
                    success: onSuccess,
                    error: onError
                });
  
                return false;
            });





        });


</script>
</body>
</html>
